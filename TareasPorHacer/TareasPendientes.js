let fs=require('fs');
let ListadoDeTareas=[];
function EncolarTareas(descripcion) {
    let tareas={
        descripcion,
        compleatedo:false
    };
    cargarBD();
    ListadoDeTareas.push(tareas);
    GuardarDB();
    return ListadoDeTareas;    
}


function GuardarDB() 
    {
        let data=JSON.stringify(ListadoDeTareas);
        fs.writeFile(`GuardarTareas/data.json`, data, (err) => 
        {
                if (err)  throw new Error("no se pudo grabar");
        });    
    };
function cargarBD() 
    {
        try {
            ListadoDeTareas=require('../GuardarTareas/data.json');
            
        } catch (error) {
            ListadoDeTareas=[]        
        }    
    }
function GetListado() {
    ListadoDeTareas=require('../GuardarTareas/data.json');
    return ListadoDeTareas;
    
}
function  ActualizarTarea(descripcion,estado ) {
    let TodasLasTareas=GetListado();
    
    for(i in TodasLasTareas)
    {
       
        if (TodasLasTareas[i].descripcion===descripcion)
        {
            TodasLasTareas[i].compleatedo=estado;
            GuardarDB();
        }
    }
    
}
function BorrarTarea(descripcion) {

    try {
        let TodasLasTareas=GetListado();
        for(i in TodasLasTareas)
        {
            if (TodasLasTareas[i].descripcion===descripcion)
            {
                TodasLasTareas.splice( i, 1 );
                GuardarDB()
            }                     
        }
        return true  
        } 
    catch{
        console.log("se produjo un error")
    }
     
    
}
 module.exports={
        EncolarTareas,
        GetListado,
        ActualizarTarea,
        BorrarTarea
        
    } 