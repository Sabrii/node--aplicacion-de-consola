const descripcion={
    demand:true,
    alias:'d',
    desc:'Descripcion'}
const completado={
    demand:true,
    default:true,
    alias:'c',
    desc:"marca como completado la tarea" 
 }
let yargs=require('yargs')
            .command('crear','se usa para crear una tarea', 
            {
                descripcion
                   
            })
            .command('actualizar','actualiza un atributo',
            {
                descripcion,
                completado
                
            })
            .command('borrar','borra un elemento',
            {
                descripcion:{
                    demand:true,
                    alias:'d',
                    desc:"borrar una tarea "
                }
            })
            .help()
            .argv;
module.exports={
    yargs
}