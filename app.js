const yargs= require('./Config/yargs').yargs;
const tareas=require('./TareasPorHacer/TareasPendientes');
const color= require('colors');

let comando=yargs._[0];


switch(comando)
{
    case 'crear':
      
        let tarea=tareas.EncolarTareas(yargs.descripcion);

        console.log("Se guardo el comentario ", tarea);
        break;
    
    case 'listar':
        let TodasLasTareas=tareas.GetListado();
        for(i in TodasLasTareas)
        {   
            console.log("=============Todas Las tareas ===============".green);
            console.log("La tarea por hacer es".red,TodasLasTareas[i].descripcion.red);
            console.log("El estado es ".red,TodasLasTareas[i].compleatedo);
            console.log("=============================================".green);
        }
        break;
    case 'actualizar':

        let actualizado =tareas.ActualizarTarea(yargs.descripcion,yargs.completado)

        console.log("Actualizar",actualizado );
        break;
    case 'borrar':
        
        let borrado=tareas.BorrarTarea(yargs.descripcion);
        
        break

    default:
        console.log("no se reconoce el comando");
}
